var PRICE=9.99;
var LOAD_NUM=10;

new Vue({
	el:'#app',
	data:{
		total:0,
		items:[],
	cart:[],
	newSearch:'90s',
	results:undefined,
	totalResults:[],
	lastSearch:'',
	loading:false,
	price:PRICE,
	},
	
	computed:{
		noMoreItems:function(){
			return this.items.length===this.totalResults.length && this.totalResults.length>0;
		}
	},
	methods:{
		appendItems: function(){
			this.items=this.items.concat(this.totalResults.slice(this.items.length,this.items.length+LOAD_NUM));
		},

		onSubmit:function(){
			if(this.newSearch==''){return;}
			this.items=[];
			this.loading=true;
			this.$http.get('/search/'.concat(this.newSearch))
			.then(function(res){
				this.totalResults=res.data;
				this.items=res.data.slice(0,LOAD_NUM);
				this.results=res.data.length;
				this.lastSearch=this.newSearch;
				this.loading=false;
			});
		},

		addItem: function(index){
			this.total+=PRICE;
			var item = this.items[index];
			var found= false;
			for(var i = 0;i<this.cart.length;i++){
				if(this.cart[i].id===item.id){
					found=true;
					this.cart[i].qty++;
					break;
				}
			}
			if(!found){
				this.cart.push({
					id:item.id,
					title:item.title,
					qty:1,
					price:PRICE
				});			
			}	
		},
		inc: function(item,index){
			item.qty++;
			this.total+=PRICE;
		},
		dec: function(item,index){
			item.qty--;
			this.total-=PRICE;
			if(item.qty<=0){
				this.cart.splice(index,1);
			}
		}
	},

	filters:{
		currency:function(price){
			return '$'.concat(price.toFixed(2));
		}
	},

	mounted:function(){
		this.onSubmit();

		var vueInstance=this;
		var myElement = document.getElementById("product-list-bottom");
		var elementWatcher = scrollMonitor.create( myElement );

		elementWatcher.enterViewport(function() {
		    vueInstance.appendItems();
		});
	}
});

